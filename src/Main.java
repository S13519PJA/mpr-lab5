
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;


public class Main {
	private static List<User> users;

	private static void init() {
		
		Person jaroslaw = new Person();
		Person szymon = new Person();
		Person janusz = new Person();
		Person andrzej = new Person();
		Person sakura = new Person();
		
		jaroslaw.setAge(33);
		jaroslaw.setName("Jaroslaw");
		jaroslaw.setSurname("Krzeczkowski");
		
		szymon.setAge(22);
		szymon.setName("Szymon");
		szymon.setSurname("Wajda");
		
		janusz.setAge(52);
		janusz.setName("Janusz");
		janusz.setSurname("Sawada");
		
		andrzej.setAge(34);
		andrzej.setName("Andrzej");
		andrzej.setSurname("Torba");
		
		sakura.setAge(22);
		sakura.setName("Sakura");
		sakura.setSurname("Crown");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		User user3 = new User("stefan", "pass123", janusz);
		User user4 = new User("endrju", "pass123", andrzej);
		User user5 = new User("saiten", "pass123", sakura);
		
		users = Arrays.asList(user1, user2, user3,user4,user5);
		
		Role roleUser = new Role();
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		janusz.setRole(roleAdmin);
		jaroslaw.setRole(roleGuest);
		sakura.setRole(roleUser);
		andrzej.setRole(roleGuest);
		szymon.setRole(roleAdmin);
		
		Address a1 = new Address("Poland", "Gdansk", "Morska", 127);
		Address a2 = new Address("Poland", "Gdynia", "Koszalinska", 15);
		Address a3 = new Address("Poland", "Gdansk", "Walowa", 27);
		
		janusz.setAddresses(Arrays.asList(a1,a2,a3));
		jaroslaw.setAddresses(Arrays.asList(a1,a2));
		sakura.setAddresses(Arrays.asList(a1));
		andrzej.setAddresses(Arrays.asList(a1));
		szymon.setAddresses(Arrays.asList(a1));
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permUser = Arrays.asList(permWrite,permRead);
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleUser.setPermissions(permUser);
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		List<User> moreThan2Addresses = UserService.findUsersWhoHaveMoreThanOneAddress(users);
		System.out.println(moreThan2Addresses);
		
		Person oldest = UserService.findOldestPerson(users);
		System.out.println("Najstarszy jest: " + oldest.getName() + " " 
							+ oldest.getSurname() + " lat " + oldest.getAge());
		User longest = UserService.findUserWithLongestUsername(users);
		System.out.println("Najdluzszy login ma: " + longest.getName());
		
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		System.out.println("Ukonczone 18 lat ma:" + result);
		
		List<String> nameStartingWithA = UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users);
		System.out.println("Posortowana lista uprawnien uzytkownikow z imieniem na A: ");
		System.out.println(nameStartingWithA);

		System.out.println("Lista uprawnien uzytkownikow z nazwiskiem na S: ");
		UserService.printCapitalizedPermissionNamesOfUsersWithSurnameStartingWithS(users);
		
		Map<Role, List<User>> groupUsersByRole = UserService.groupUsersByRole(users);
		System.out.println(groupUsersByRole);
		
		Map<Boolean, List<User>> groupUsersBy18 = UserService.partitionUserByUnderAndOver18(users);
		System.out.println(groupUsersBy18);
	
	}
	


	public static void main(String[] args) {
		init();
	
	}
}
