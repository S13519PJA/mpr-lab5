package tests;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import domain.Address;
import domain.Permission;
import domain.Person;
import domain.Role;
import domain.User;
import service.UserService;

public class UserSeviceTest {

	@Test
	public void testFindUsersWhoHaveMoreThanOneAddress() {
		
		Person p = new Person();
		Person p2 = new Person();
		
		User u = new User("name", "Password", p);
		User userWith2Addresses = new User("name2", "haslo", p2);
		
		Address a1 = new Address("Poland", "Gdansk", "Morska", 127);
		Address a2 = new Address("Poland", "Gdynia", "Koszalinska", 15);
		
		p.setAddresses(Arrays.asList(a1));
		p2.setAddresses(Arrays.asList(a1,a2));
		
		u.setPersonDetails(p);
		userWith2Addresses.setPersonDetails(p2);
		
		List<User> users = new ArrayList<User>();
		users.add(u);
		users.add(userWith2Addresses);

		List<User> result = UserService.findUsersWhoHaveMoreThanOneAddress(users);

		assertSame(result.get(0), userWith2Addresses); 
		
	}

	@Test
	public void testFindOldestPerson() {
		List<User> users;

		Person jaroslaw = new Person();
		Person szymon = new Person();
		Person janusz = new Person();
		jaroslaw.setAge(23);
		szymon.setAge(22);
		janusz.setAge(52);
		janusz.setName("janusz");
		janusz.setSurname("Sawada");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		User user3 = new User("stefan", "pass123", janusz);

		users = Arrays.asList(user1, user2, user3);
		
		Person result = UserService.findOldestPerson(users);
		assertSame(82, result.getAge());
		
	}

	@Test
	public void testFindUserWithLongestUsername() {
		List<User> users;

		Person jaroslaw = new Person();
		Person szymon = new Person();
		Person janusz = new Person();
		jaroslaw.setAge(33);
		szymon.setAge(22);
		janusz.setAge(52);
		janusz.setName("janusz");
		janusz.setSurname("Sawada");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		User user3 = new User("stefan", "pass123", janusz);

		users = Arrays.asList(user1, user2, user3);
		User result = UserService.findUserWithLongestUsername(users);
		assertSame("saviola", result.getName());
	}

	@Test
	public void testGetNamesAndSurnamesCommaSeparatedOfAllUsersAbove18() {
		
		List<User> users;
		
		Person jaroslaw = new Person();
		Person szymon = new Person();
		Person janusz = new Person();
		
		jaroslaw.setAge(33);
		jaroslaw.setName("Jaroslaw");
		jaroslaw.setSurname("Krzeczkowski");
		
		szymon.setAge(22);
		szymon.setName("Szymon");
		szymon.setSurname("Wajda");
		
		janusz.setAge(52);
		janusz.setName("Janusz");
		janusz.setSurname("Sawada");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		User user3 = new User("stefan", "pass123", janusz);
		
		users = Arrays.asList(user1, user2, user3);
		String result = UserService.getNamesAndSurnamesCommaSeparatedOfAllUsersAbove18(users);
		
		assertEquals("Jaroslaw Krzeczkowski,Szymon Wajda,Janusz Sawada", result);
	}
	

	@Test
	public void testGetSortedPermissionsOfUsersWithNameStartingWithA() {
		
		Person jaroslaw = new Person();
		Person szymon = new Person();
		
		jaroslaw.setAge(33);
		jaroslaw.setName("Jaroslaw");
		jaroslaw.setSurname("Krzeczkowski");
		
		szymon.setAge(22);
		szymon.setName("Szymon");
		szymon.setSurname("Wajda");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		
		List<User> users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		jaroslaw.setRole(roleGuest);
		szymon.setRole(roleAdmin);
		
		Permission permWrite = new Permission();
		Permission permRead = new Permission();
		Permission permExecute = new Permission();
		
		permWrite.setName("write");
		permRead.setName("read");
		permExecute.setName("execute");
		
		List<Permission> permGuest = Arrays.asList(permRead);
		List<Permission> permAdmin = Arrays.asList(permWrite,permRead,permExecute);
		
		roleGuest.setPermissions(permGuest);
		roleAdmin.setPermissions(permAdmin);
		
		assertEquals(Arrays.asList("read"),UserService.getSortedPermissionsOfUsersWithNameStartingWithA(users));
		
	}

	@Test
	public void testGroupUsersByRole() {
		List<User> users;
		
		Person jaroslaw = new Person();
		Person szymon = new Person();
		
		jaroslaw.setAge(33);
		jaroslaw.setName("Jaroslaw");
		jaroslaw.setSurname("Krzeczkowski");
		
		szymon.setAge(22);
		szymon.setName("Szymon");
		szymon.setSurname("Wajda");
		
		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		List<User> guestList = Arrays.asList(user1);
		List<User> adminList = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		jaroslaw.setRole(roleGuest);
		szymon.setRole(roleAdmin);
		
		Map<Role, List<User>> result = UserService.groupUsersByRole(users);
		Map<Role, List<User>> test = new HashMap<Role, List<User>>();
		test.put(roleGuest, guestList );
		test.put(roleAdmin, adminList);
		assertEquals(test,result);
	}
	

	@Test
	public void testPartitionUserByUnderAndOver18() {
List<User> users;
		
		Person jaroslaw = new Person();
		Person szymon = new Person();

		jaroslaw.setAge(15);
		jaroslaw.setName("Jaroslaw");
		jaroslaw.setSurname("Krzeczkowski");

		szymon.setAge(22);
		szymon.setName("Szymon");
		szymon.setSurname("Wajda");

		User user1 = new User("jarek", "zabpw123", jaroslaw);
		User user2 = new User("smon", "passwd1", szymon);
		List<User> under18List = Arrays.asList(user1);
		List<User> over18List = Arrays.asList(user2);
		users = Arrays.asList(user1, user2);
		
		Role roleGuest = new Role();
		Role roleAdmin = new Role();
		
		jaroslaw.setRole(roleGuest);
		szymon.setRole(roleAdmin);
		
		Map<Boolean, List<User>> result = UserService.partitionUserByUnderAndOver18(users);
		Map<Boolean, List<User>> test = new HashMap<Boolean, List<User>>();
		test.put(false, under18List );
		test.put(true, over18List);
		assertEquals(test,result);
	}

}
